
# Requirements
### Docker



# Configuration 
```
cp env-sample .env
docker-compose up -d --build
```

## Then run following commands
```
docker-compose exec app composer install
```
### Or
If you have PHP 8 you can use instead of the previous one
```
composer install -d ./WunderMobility
```
# Database
## Create tables
```
docker-compose exec app php database/migration/db.php
```
### To see the database and tables
```
docker-compose exec mysql mysql -u wunder_mobility_user -psecret
show databases;
use wunder_mobility;
show tables;
```

# Web Page Url
## landing page
### http://localhost

# Backend App Url
### This page shows some info about the device
http://localhost:8080

# APIs 
## register
### Saves the given data 
Url POST http://localhost:8080/api/registers
param:
```
{
    "user" :{
        "firstname" : "dariush",
        "lastname" : "Moh",
        "telephone": "9999999",
        "email": "Dar@dar.ca"
    },
    "address":{
        "houseNo" : "n34",
        "street" : "Moh 156",
        "city": "lyngby",
        "zipCode": "12345"
    },
    "payment" :{
        "accountOwner": "765456",
        "iban": "12345"
    }
    
}
```

### Response
```
{
    "paymentDataId": "bc71294cf274d19c1534aace351d1e1284a783e91db949c294c5075232b12716d0dd44a0c2163215be08f9eac28ff73f"
}
```
