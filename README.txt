1. Describe possible performance optimizations for your Code.

In order to optimise my code, I would apply cache to improve data retrieval performance and I would use ORM technique to optimise the query code. Using queue is also another option I would consider to handle heavy load.


2. Which things could be done better, than you’ve done it?

Validation in frontend could be done better since I applied a very simple validation.
In the backend, I strongly believe there should be validation which I did not include since it was not part of the requirement.
Unit-testing was not part of the requirement and I did not write it, but in my opinion, it should always be part of the work to improve the overall quality.
Strategy pattern is a design pattern which I would have used if this project was supposed to extend or to be used in real world.
Lastly, useContext is what I could use in the frontend instead of using collect class.