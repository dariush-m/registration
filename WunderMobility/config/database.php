<?php

return array(
    'host' => 'mysql',
    'username' => 'wunder_mobility_user',
    'password' => 'secret',
    'dbname'   => 'wunder_mobility',
    'charset' => 'utf8mb4',
    'port' => '3306'
);