<?php 
use Registration\Route\Route;
use Registration\Route\Api;
use Registration\Route\Web;


$route = new route();

if ($route->isApi()) 
	(new Api)->getControllers();

if ($route->isWeb()) 
	die('please use this api-> post /api/registers');