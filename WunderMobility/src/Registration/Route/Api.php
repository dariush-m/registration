<?php
namespace Registration\Route;

use Registration\Route\RouteBase\RouteBase;
use Registration\Controller\RegisterController;


class Api extends RouteBase
{
	function __construct()
	{
		parent::__construct();
		header( 'Content-Type: application/json' );
	}

	public function getControllers() : void
	{
		$this->server->addClass(RegisterController::class);
	}
}