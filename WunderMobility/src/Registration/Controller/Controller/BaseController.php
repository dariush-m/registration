<?php
namespace Registration\Controller\Controller;

use Registration\Controller\Controller\ErrorHandler;

class BaseController
{
	use ErrorHandler;
}