<?php
namespace Registration\Controller\Controller;

trait ErrorHandler {
    function error($error, $status = null) : array
    { 
    	return ['erorr' => [
    		'message' => $error->getMessage(),
    		'code' => $status ?? $error->getCode()
    	]];
    }
}