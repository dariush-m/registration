<?php 
namespace Registration\Controller;

use Registration\Model\Client\Client;
use Registration\Model\Payment\Payment;
use Registration\Model\Address\Address;
use Registration\Model\User\User;
use \Jacwright\RestServer\RestException;
use Registration\Controller\Controller\BaseController;
use Registration\Model\Client\ClientInterface;
use Registration\Model\Register\Register;


class RegisterController extends BaseController
{ 
    /**
     * Test
     *
     * @url GET /api/register
     * 
     */
    public function show() 
    {
        return 'hello!!!';
        // return $this->checkPayment(new Client);
        // return $this->user->getUsers();
        // return $this->address->getAddresses();
        // return $this->payment->getPayments();
    }
    

    /**
     * Saves an ad to the database
     *
     * @url POST /api/registers
     * @url PUT /api/registers/$id
     */
    public function registeration(int $id = null, $data) 
    {
        try{
            
            $response = $this->checkPayment(new Client);
            $statusCode = (int)mb_substr($response['statusCode'], 0 , 1);
            if( $statusCode === 4)
                throw new \Exception('Paymet has been failed.');
            
            $this->register()->save($data);
            return $response['data'];
            
        }catch (Throwable $e){
            return ['message'=>$e];
        }catch (InvalidArgumentException $e){
            return $this->error($e, 404);
        }catch (DbException $e){
            return $this->error($e, 404);
        }
    }


    /**
     * Delete ad by id
     *            
     * @url DELETE /api/registers/$id
     */
    public function deleteUser(int $id)
    {
        //
    }

    /**
     * Throws an error
     * 
     * @url GET api/error
     */
    public function throwError() {
        throw new RestException(401, 'Empty password not allowed');
    }

    private function checkPayment(ClientInterface $client){
        $url ='https://37f32cl571.execute-api.eu-central-1.amazonaws.com/default/wunderfleet-recruiting-backend-dev-save-payment-data';
        $data = [
            'customerId' => '1',
            'iban' => 'DE8234',
            'owner' => 'Max Mustermann'
        ];
        
        $headers = [
            'Content-Type: application/json'
        ];

        return $client->post($url, $headers, $data);
    }

    private function register(){
        return (new Register(new User, new Address, new Payment));
    }
}
