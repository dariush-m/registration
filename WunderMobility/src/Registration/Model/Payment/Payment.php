<?php
namespace Registration\Model\Payment;

use Exception;
use InvalidArgumentException;
use Registration\Exception\DbException;
use Registration\Model\BaseModel\BaseModel;

class Payment extends BaseModel 
{
    public function getPayment(int $id) : array
    {
        //
    }


    public function getPayments() : array
    {
        return ['data' => $this->conn->query("SELECT * FROM payments")->fetchAll()];
    }

    public function save($data) : void
    {
        $payment = $data->payment;
        try{
            if (!isset($payment->accountOwner) 
                || !isset($payment->iban)
                || !isset($data->userId)) 
                    throw new InvalidArgumentException('Invalid request.');
            
                $sql = "INSERT INTO payments (account_owner, iban, user_id) VALUES ('$payment->accountOwner', '$payment->iban', '$data->userId')";
                if (!$this->conn->exec($sql)) 
                    throw new DbException($this->conn->errorInfo());
                
        }catch (Throwable $e){
            throw new Exception($e->getMessage());
        }
    }


    public function update() : array
    {
    
    }

    public function delete(int $id) : array
    {
        //
    }
}