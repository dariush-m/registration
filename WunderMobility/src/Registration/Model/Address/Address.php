<?php
namespace Registration\Model\Address;

use Exception;
use InvalidArgumentException;
use Registration\Exception\DbException;
use Registration\Model\BaseModel\BaseModel;

class Address extends BaseModel 
{
	public function getUserAddress(int $id) : array
    {
        //
    }

    public function getAddresses() : array
    {
        return ['data' => $this->conn->query("SELECT * FROM addresses")->fetchAll()];
    }

    public function save($data) : void
    {
        $address = $data->address;
        try{
            if (!isset($address->houseNo) 
                || !isset($address->street) 
                || !isset($address->city)
                || !isset($data->userId)) 
                    throw new InvalidArgumentException('Invalid request.');
            
                $sql = "INSERT INTO addresses (house_no, street, city, zip_code, user_id) VALUES ('$address->houseNo', '$address->street', '$address->city', '$address->zipCode', '$data->userId')";
                if (!$this->conn->exec($sql)) 
                    throw new DbException($this->conn->errorInfo());
                
        }catch (Throwable $e){
            throw new Exception($e->getMessage());
        }
    }

    public function update() : array
    {
        //
    }

    public function delete(int $id) : array
    {
        //
    }
}