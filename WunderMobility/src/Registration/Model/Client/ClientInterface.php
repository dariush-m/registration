<?php
namespace Registration\Model\Client;

interface ClientInterface
{
    public function post(string $url, array $headers, array $data);
}
