<?php
namespace Registration\Model\Client;

use Registration\Model\Client\ClientInterface;

class Client implements ClientInterface
{ 
    public $client;
    
    function __construct(){
        $this->client = curl_init();
    }

    public function post( string $url, array $headers, array $data) : array
    {
        curl_setopt($this->client, CURLOPT_URL, $url);
        curl_setopt($this->client, CURLOPT_POST, 1);
        curl_setopt($this->client, CURLOPT_RETURNTRANSFER, 1);
        curl_setopt($this->client, CURLOPT_POSTFIELDS, json_encode($data));           
        curl_setopt($this->client, CURLOPT_HTTPHEADER, $headers);

        return [
            'data' => json_decode(curl_exec($this->client), true),
            'statusCode' => curl_getinfo($this->client, CURLINFO_HTTP_CODE)
        ];
    }
}