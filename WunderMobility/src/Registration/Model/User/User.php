<?php
namespace Registration\Model\User;

use Registration\Model\BaseModel\BaseModel;
use \Jacwright\RestServer\RestException;
use Registration\Exception\DbException;
use InvalidArgumentException;
use Exception; 

class User extends BaseModel 
{
	public function getUser(int $id) : array
    {
        //
    }

    public function getUsers() : array
    {
        return ['data' => $this->conn->query("SELECT * FROM users")->fetchAll()];
    }

    public function save($data) : int
    {
        $user = $data->user;
        try{
            if (!isset($user->firstname) 
                || !isset($user->lastname) 
                || !isset($user->telephone)
                || !isset($user->email)) 
                    throw new InvalidArgumentException('Invalid request.');
            
                $sql = "INSERT INTO users (firstname, lastname, telephone, email) VALUES ('$user->firstname', '$user->lastname', '$user->telephone', '$user->email')";
                if (!$this->conn->exec($sql)) 
                    throw new DbException($this->conn->errorInfo());
                
            return $this->conn->lastInsertId();
        }catch (Throwable $e){
            throw new Exception($e->getMessage());
        }
    }

    public function update() : array
    {
        //
    }


    public function delete(int $id) : array
    {
        //
    }
}