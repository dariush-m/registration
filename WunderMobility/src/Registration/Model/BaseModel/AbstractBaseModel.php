<?php
namespace Registration\Model\BaseModel;

use Exception;

/**
 * Class AbstractBaseModel
 * @package Registration\Model\BaseModel
 */
abstract class AbstractBaseModel

{
    /**
     * AbstractBaseModel constructor.
     */
    function __construct() {
        $this->setMySQLConnection();
    }

    /**
     *
     */
    private function setMySQLConnection() : void
    {
        $host       = $GLOBALS['config']['host'];
        $dbname     = $GLOBALS['config']['dbname'];
        $username   = $GLOBALS['config']['username'];
        $password   = $GLOBALS['config']['password'];
        $port       = $GLOBALS['config']['port'];
        $charset    =  $GLOBALS['config']['charset'];
        $options    = [
            \PDO::ATTR_ERRMODE            => \PDO::ERRMODE_EXCEPTION,
            \PDO::ATTR_DEFAULT_FETCH_MODE => \PDO::FETCH_ASSOC,
            \PDO::ATTR_EMULATE_PREPARES   => false,
        ];
        $dsn        = "mysql:host=$host;dbname=$dbname;charset=$charset;port=$port";
        try{
            $conn   = new \PDO($dsn, $username, $password, $options);
            // echo 'Connected succesfully!!! \n';  
            $this->conn = $conn;      
        } catch (\PDOException $e) {
            throw new \PDOException($e->getMessage(), (int)$e->getCode());
        }  
    }

    protected function tableExists(string $table) : bool 
    {
        try {
            $result = $this->conn->query("SELECT 1 FROM $table LIMIT 1");
        } catch (Exception $e) {
            return FALSE;
        }
        return $result !== FALSE;
    }

    /**
     *
     */
    function __destruct() 
    {
        $this->conn = null;
    }
}