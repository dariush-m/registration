<?php
namespace Registration\Model\BaseModel;

use Exception;

/**
 * Class BaseModel
 * @package Registration\Model\BaseModel
 */
class BaseModel extends AbstractBaseModel
{
    /**
     * BaseModel constructor.
     */
    function __construct()
	{
		parent::__construct();
	}
}