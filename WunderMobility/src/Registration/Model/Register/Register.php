<?php
namespace Registration\Model\Register;

use Registration\Model\BaseModel\BaseModel;
use \Jacwright\RestServer\RestException;
use Registration\Exception\DbException;
use Registration\Model\Payment\Payment;
use Registration\Model\Address\Address;
use Registration\Model\User\User;
use InvalidArgumentException;
use Exception; 

class Register extends BaseModel 
{
	protected $user;
    protected $address;
    protected $payment;

    function __construct(User $user,Address $address, Payment $payment){
        $this->user = $user;
        $this->address = $address;
        $this->payment = $payment;
    }

    public function save($data) : void
    {
        // $this->conn->beginTransaction();
        try{
            
            $data->userId = $this->user->save($data);
            $this->address->save($data);
            $this->payment->save($data);
            // $this->conn->commit();

        }catch (Throwable $e) {
            // $this->conn->rollBack();
            throw new PDOException($e->getMessage());
        }
        
        
    }

}