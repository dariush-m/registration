<?php
require_once __DIR__ . '/../requirements/requirement.php';

use Registration\Model\BaseModel\AbstractBaseModel;


class Db extends AbstractBaseModel
{
    private function execute(string $sql, string $table){
        try{  
            $this->conn->exec($sql);
            echo "$table table has been created! \n";
        }
        catch (PDOException $e) {
            echo $e->getMessage();
        }
    }

    public function createUsersTable()
    {
        $sql = "create table users(
            id INT NOT NULL AUTO_INCREMENT,
            firstname VARCHAR(30) NOT NULL,
            lastname VARCHAR(30) NOT NULL,
            email VARCHAR(255) NOT NULL,
            telephone VARCHAR(16) NOT NULL,
            primary key (id)
        ) 
        COLLATE='utf8_general_ci'
        ENGINE=INNODB";  
        
        if($this->tableExists('users'))
            echo "User table already exist! \n";
        else
            $this->execute($sql, 'User');
    }

    public function createAddressesTable()
    {
        $sql = "create table addresses(
            id INT NOT NULL AUTO_INCREMENT,
            house_no VARCHAR(20) NOT NULL,
            street VARCHAR(255) NOT NULL,
            city VARCHAR(80) NOT NULL,
            zip_code VARCHAR(10),
            user_id INT NOT NULL,
            primary key (id),
            FOREIGN KEY (user_id) REFERENCES users(id) ON DELETE CASCADE
        ) 
        COLLATE='utf8_general_ci'
        ENGINE=INNODB";  
        
        if($this->tableExists('addresses'))
            echo "Address table already exist! \n"; 
        else
            $this->execute($sql, 'Address');
    }

    public function createPaymentsTable()
    {
        

        $sql = "create table payments (
            id INT NOT NULL AUTO_INCREMENT,
            account_owner VARCHAR(60) NOT NULL,
            iban VARCHAR(60) NOT NULL,
            user_id INT NOT NULL,
            primary key (id),
            FOREIGN KEY (user_id) REFERENCES users(id) ON DELETE CASCADE
        ) 
        COLLATE='utf8_general_ci'
        ENGINE=INNODB";  
        
            if($this->tableExists('payments'))
                echo "Payment table already exist! \n";
            else
                $this->execute($sql, 'Payment');
            
    }
}
$db = new DB();
$db->createUsersTable();
$db->createAddressesTable();
$db->createPaymentsTable();

die();